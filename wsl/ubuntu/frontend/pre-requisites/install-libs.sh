#!/bin/bash

#########################################
#                                       #
#   Install libs for angular projects   #
# last version of angular cli and node  #
# v3.6 using nvm. Also install nvm.     #
# Requires npm                          #
#                                       #
#########################################


#install node
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
source ~/.bashrc
nvm install exitv15.0.1

#install angnular cli

npm install -g @angular/cli
