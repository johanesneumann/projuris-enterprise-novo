#!/bin/bash

GREEN='\033[0;32m'
NC='\033[0m'

if [ $# -qe 0 ]
  then
    echo "Source folder path not provided, exiting. Use: setup-frontend-src-folder [path]"
    exit 1
fi

local frontEndSourceDir=$1
echo $1
user=$2

printf "${GREEN}"
echo "Setting up frontend source folder to $(pwd)/$frontEndSourceDir"
printf "${NC}"

mkdir -p $frontEndSourceDir

printf "${GREEN}"
echo "Source folder created"