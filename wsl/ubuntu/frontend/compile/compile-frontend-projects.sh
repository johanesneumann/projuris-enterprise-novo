#!/bin/bash

#########################################
#                                       #
#   Compile all projects, with          #
#   yarn install. Ignoring tests        #
#                                       #
#########################################

GREEN='\033[0;32m'
NC='\033[0m' 

clonedFrontEndDir=$1
atual=$(pwd)


printf "${GREEN}"
echo "Compiling frontend projects:"
if ! [ -z $clonedFrontEndDir ]
  then
    cd $clonedFrontEndDir
    echo "In directory: $clonedFrontEndDir"    
fi

echo " - projuris-enterprise-app"

printf "${NC}"

cd projuris-enterprise-app
yarn install

if [ $clonedFrontEndDir ]
  then
    cd $actual
fi

printf "${GREEN}"
echo "Compiling DONE"
printf "${NC}"