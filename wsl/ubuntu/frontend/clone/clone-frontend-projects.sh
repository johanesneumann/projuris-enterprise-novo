#!/bin/bash


#########################################
#                                       #
# Clone all projects from remote repos  #
#         in their default branch       #
#                                       #
#########################################


GREEN='\033[0;32m'
NC='\033[0m' 

destinationDirFrontSource=$1

printf "${GREEN}"

atualFrontCloneDir=$(pwd)

echo "Cloning frontend repos:"
if ! [ -z $destinationDirFrontSource ]
  then
    cd $destinationDirFrontSource
    echo "To directory: $destinationDirFrontSource"    
fi


echo " - projuris-enterprise-app"
printf "${NC}"



git clone git@bitbucket.org:projuris_enterprise/projuris-enterprise-app.git

if ! [ -z $destinationDirFrontSource ]
  then
    cd $atualFrontCloneDir
fi
printf "${GREEN}"
echo "Cloning DONE"
printf "${NC}"