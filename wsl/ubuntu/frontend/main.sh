#!/bin/bash

CONFIG_ENV_SCRIPT='../config/set-configs-env.sh'
LIBS_FRONTEND='./pre-requisites/install-libs.sh'
SETUP_FRONTEND_SOURCE_DIR='./setup/setup-frontend-src-folder.sh'
CLONE_FRONTEND_SRC_SCRIPT='./clone/clone-frontend-projects.sh'
COMPILE_FRONTEND_PROJECT='./compile/compile-frontend-projects.sh'

echo "Definindo configuracoes padrao:"
source "$CONFIG_ENV_SCRIPT"

source "$LIBS_FRONTEND"

source "$SETUP_FRONTEND_SOURCE_DIR" $frontEndSourceDir
source "$CLONE_FRONTEND_SRC_SCRIPT" $frontEndSourceDir
source "$COMPILE_FRONTEND_PROJECT" $frontEndSourceDir