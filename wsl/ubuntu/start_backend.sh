#!/bin/bash

CONFIG_ENV_SCRIPT='./config/set-configs-env.sh'

echo "Definindo configuracoes padrao:"
source "$CONFIG_ENV_SCRIPT"

cd $backendSourceDir/docker-compose
sudo service docker start
sudo docker-compose -f config-essential-1.yml up -d
read -p "Aperte qualquer tecla para continuar quando postgres estiver aceitando conexoes (30~50s depois desta mensagem aparecer, normalmente)"
sudo docker-compose -f config-essential-2.yml up -d
sudo docker-compose -f config-projuris.yml up -d --build
docker attach enterprise-api


