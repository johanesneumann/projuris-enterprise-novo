#!/bin/bash

GREEN='\033[0;32m'
NC='\033[0m'

if [ $# -qe 0 ]
  then
    echo "Source folder path not provided, exiting. Use: setup-backend-src-folder [path]"
    exit 1
fi

sourceDir=$1
echo $1
user=$2

printf "${GREEN}"
echo "Setting up backend source folder to $(pwd)/$sourceDir"
printf "${NC}"

mkdir -p $sourceDir

printf "${GREEN}"
echo "Source folder created"