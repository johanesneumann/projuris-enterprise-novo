#!/bin/bash


#########################################
#                                       #
# Clone all projects from remote repos  #
#         in their default branch       #
#                                       #
#########################################


GREEN='\033[0;32m'
NC='\033[0m' 

destinationDir=$1

printf "${GREEN}"

atual=$(pwd)

echo "Cloning backend repos:"
if ! [ -z $destinationDir ]
  then
    cd $destinationDir
    echo "To directory: $destinationDir"    
fi


echo " - data-service"
echo " - docker-compose"
echo " - enterprise-auth-service"
echo " - functionalityid-maven-plugin"
echo " - gateway-service"
echo " - notification-user-service"
echo " - projuris-domain"
echo " - projuris-enterprise-api"
echo " - projuris-spring"
echo " - report-service"
echo " - scheduler-service"
echo " - variable-replacement-service"
echo " - websocket-service"
echo " - auditoria-service"
printf "${NC}"



git clone git@bitbucket.org:projuris_enterprise/data-service.git
git clone git@bitbucket.org:projuris_enterprise/docker-compose.git
git clone git@bitbucket.org:projuris_enterprise/enterprise-auth-service.git
git clone git@bitbucket.org:projuris_enterprise/functionalityid-maven-plugin.git
git clone git@bitbucket.org:projuris_enterprise/gateway-service.git
git clone git@bitbucket.org:projuris_enterprise/notification-user-service.git
git clone git@bitbucket.org:projuris_enterprise/projuris-domain.git
git clone git@bitbucket.org:projuris_enterprise/projuris-enterprise-api.git
git clone git@bitbucket.org:projuris_enterprise/projuris-spring.git
git clone git@bitbucket.org:projuris_enterprise/report-service.git
git clone git@bitbucket.org:projuris_enterprise/scheduler-service.git
git clone git@bitbucket.org:projuris_enterprise/variable-replacement-service.git
git clone git@bitbucket.org:projuris_enterprise/websocket-service.git
git clone git@bitbucket.org:projuris_enterprise/auditoria-service.git

if ! [ -z $destinationDir ]
  then
    cd $actual
fi
printf "${GREEN}"
echo "Cloning DONE"
printf "${NC}"