#!/bin/bash

#########################################
#                                       #
#   Compile all projects, with maven    #
#   clean install, ignoring tests       #
#                                       #
#########################################

compileMavenProject(){
  local projectName=$1
  cd $1
  printf "${GREEN}"
  echo "Compiling $projectName"
  printf "${NC}"
  mvn clean | grep "BUILD SUCCESS"
  mvn install -DskipTests=true | grep "BUILD SUCCESS"
}

GREEN='\033[0;32m'
NC='\033[0m' 

destinationDir=$1
atual=$(pwd)


printf "${GREEN}"
echo "Compiling backend projects:"
if ! [ -z $destinationDir ]
  then
    cd $destinationDir
    echo "In directory: $destinationDir"    
fi
echo " - projuris-spring"
echo " - projuris-domain"
echo " - functionalityid-maven-plugin"
echo " - projuris-enterprise-api"
echo " - data-service"
echo " - enterprise-auth-service"
echo " - gateway-service"
echo " - notification-user-service"
echo " - report-service"
echo " - scheduler-service"
echo " - variable-replacement-service"
echo " - websocket-service"
echo " - auditoria-service"
printf "${NC}"

compileMavenProject 'projuris-spring'

compileMavenProject '../projuris-domain'

compileMavenProject  '../functionalityid-maven-plugin'

compileMavenProject  '../projuris-enterprise-api'

compileMavenProject  '../data-service'

compileMavenProject  '../enterprise-auth-service'

compileMavenProject  '../gateway-service'

compileMavenProject  '../notification-user-service'

compileMavenProject  '../report-service'

compileMavenProject  '../scheduler-service'

compileMavenProject  '../variable-replacement-service'

compileMavenProject  '../websocket-service'

compileMavenProject  '../auditoria-service'

if ! [ -z $destinationDir ]
  then
    cd $actual
fi

printf "${GREEN}"
echo "Compiling DONE"
printf "${NC}"