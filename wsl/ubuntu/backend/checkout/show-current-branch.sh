#!/bin/bash

cd $1

dir=${PWD}
GREEN='\033[0;32m'
NC='\033[0m' # No Color

for file in "$dir/"*
do
  if [ -d "$file/.git" ]; then
    echo ""
    echo "$file"
          cd "$file"
          printf "${GREEN}"
          git branch --show-current
          printf "${NC}"
  fi
done
