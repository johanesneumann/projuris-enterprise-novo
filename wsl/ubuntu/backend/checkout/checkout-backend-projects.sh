#!/bin/bash

##############################################################################
#                                                                            #
#   Change branch of all projects,                                           #
#   when the branch is avaliable                                             #
#   Usage:                                                                   #
# bash checkout-backend-projects.sh sourceDir targetBranch defaultBranch     #
#                                                                            #
##############################################################################


CHECKOUT_BRANCH='./../utils/checkout-branch.sh'
SHOW_CURRENT_BRANCHES='./checkout/show-current-branch.sh'


RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' 

checkoutBackendSourceDir=$1
targetBranch=$2
defaultBranch=$3

currentDir=$(pwd)

printf "${GREEN}"
echo "Checking out backend projects:"

echo " - data-service"
echo " - docker-compose"
echo " - enterprise-auth-service"
echo " - functionalityid-maven-plugin"
echo " - gateway-service"
echo " - notification-user-service"
echo " - projuris-domain"
echo " - projuris-enterprise-api"
echo " - projuris-spring"
echo " - report-service"
echo " - scheduler-service"
echo " - variable-replacement-service"
echo " - websocket-service"
echo " - auditoria-service"
printf "${NC}"

echo $(pwd)
source "$CHECKOUT_BRANCH" "$checkoutBackendSourceDir/projuris-spring" $targetBranch $defaultBranch

source "$CHECKOUT_BRANCH" "$checkoutBackendSourceDir/projuris-domain" $targetBranch $defaultBranch

source "$CHECKOUT_BRANCH" "$checkoutBackendSourceDir/projuris-enterprise-api" $targetBranch $defaultBranch

source "$CHECKOUT_BRANCH" "$checkoutBackendSourceDir/data-service" $targetBranch $defaultBranch

source "$CHECKOUT_BRANCH" "$checkoutBackendSourceDir/enterprise-auth-service" $targetBranch $defaultBranch

source "$CHECKOUT_BRANCH" "$checkoutBackendSourceDir/functionalityid-maven-plugin" $targetBranch $defaultBranch

source "$CHECKOUT_BRANCH" "$checkoutBackendSourceDir/gateway-service" $targetBranch $defaultBranch

source "$CHECKOUT_BRANCH" "$checkoutBackendSourceDir/notification-user-service" $targetBranch $defaultBranch

source "$CHECKOUT_BRANCH" "$checkoutBackendSourceDir/report-service" $targetBranch $defaultBranch

source "$CHECKOUT_BRANCH" "$checkoutBackendSourceDir/scheduler-service" $targetBranch $defaultBranch

source "$CHECKOUT_BRANCH" "$checkoutBackendSourceDir/variable-replacement-service" $targetBranch $defaultBranch

source "$CHECKOUT_BRANCH" "$checkoutBackendSourceDir/websocket-service" $targetBranch $defaultBranch

source "$CHECKOUT_BRANCH" "$checkoutBackendSourceDir/auditoria-service" $targetBranch $defaultBranch


echo "Projects are in these branches now:"
source "$SHOW_CURRENT_BRANCHES" $checkoutBackendSourceDir
cd $currentDir

printf "${GREEN}"
echo "Checkout DONE"
printf "${NC}"
