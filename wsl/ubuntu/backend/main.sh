#!/bin/bash

CONFIG_ENV_SCRIPT='../config/set-configs-env.sh'
SETUP_SCRIPT='./setup/setup-backend-src-folder.sh'
CLONE_BACKEND_SRC_SCRIPT='./clone/clone-backend-projects.sh'
CHECKOUT_BACKEND_PROJECTS='./checkout/checkout-backend-projects.sh'
COMPILE_BACKEND_SRC_SCRIPT='./compile/compile-backend-projects.sh'


echo "Definindo configuracoes padrao:"
source "$CONFIG_ENV_SCRIPT"


#source "$SETUP_SCRIPT" $backendSourceDir
#source "$CLONE_BACKEND_SRC_SCRIPT" $backendSourceDir
source "$CHECKOUT_BACKEND_PROJECTS" $backendSourceDir $targetBackendBranch $defaultBackendBranch 
source "$COMPILE_BACKEND_SRC_SCRIPT" $backendSourceDir