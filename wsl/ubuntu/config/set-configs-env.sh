#!/bin/bash

#########################################
#                                       #
#   Setup all configs as environment    #
#   variables, so they can be used by   #
#   any script. Echoes the used values  #
#                                       #
#########################################

export backendSourceDir=~/dev/projuris/enterprise-backend/
echo "backendSourceDir=$backendSourceDir"

export frontEndSourceDir=~/dev/projuris/enterprise-frontend/
echo "frontEndSourceDir=$frontEndSourceDir"

export defaultBackendBranch=release
echo "defaultBackendBranch=$defaultBackendBranch"

export targetBackendBranch=epic/dashboard
echo "targetBackendBranch=$targetBackendBranch"

