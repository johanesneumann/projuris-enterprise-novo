#!/bin/bash

############################################################
#                                                          #
# Checkout a primary branch, or a                          #
# default branch if the primary dont                       #
# exist.                                                   #
#                                                          #
# Usage: bash checkout-branch primaryBranch defaultBranch  #
# Obs: Default branch is optional.                         #
#                                                          #
############################################################


RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' 

if [ $# -le 1 ] # o diretorio e a branch primaria sao obrigatorios
  then
    printf "${NC}"
    echo "################################################################"
    printf "${RED}"
    echo "# Missing arguments!                                           #"
    printf "${NC}"
    echo "# Usage:                                                       #"
    echo "# bash checkout-branch dir primaryBranch defaultBranch         #"
    echo "# Obs: Default branch is optional.                             #"
    echo "################################################################"
    printf "${NC}"
    exit 1
fi

checkoutBranchSourceDir=$1
targetBranch=$2
defaultBranch=$3

checkoutBranchCurrentDir=$(pwd)

printf "${GREEN}"
cd $checkoutBranchSourceDir
echo "Changing branch of ${PWD##*/}"
printf "${NC}"
# atualiza as referencias de commits e branchs do remoto
#git fetch --prune
# se a branch de destino existir
if ! [ -z $(git rev-parse -q --verify origin/$targetBranch) ] 
then # faz o checkout e pull
  printf "${GREEN}"
  echo "To branch: $targetBranch"
  printf "${NC}"
  git checkout $targetBranch 
  printf "${GREEN}"
  echo "Pulling lattest sources"
  printf "${NC}"
  git pull
elif ! [ -z $defaultBranch  ] 
then # senão, faz checkout e pull da branch padrao
  printf "${RED}"
  echo "Branch : $targetBranch dont exists! Changing to default branch"
  printf "${GREEN}"
  echo "Changed to default branch: $defaultBranch"
  printf "${NC}"
  git checkout $defaultBranch
  printf "${GREEN}"
  echo "Pulling lattest sources"
  printf "${NC}"
  git pull
  printf "${GREEN}"
fi

printf "${NC}"
cd $checkoutBranchCurrentDir
